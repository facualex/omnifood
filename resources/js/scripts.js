$(document).ready(function() {
  /* For sticky navigation */
  $(".js--section-features").waypoint(
    function(direction) {
      if (direction === "down") {
        $("nav").addClass("sticky");
      } else {
        $("nav").removeClass("sticky");
      }
    },
    { offset: "120px" }
  );

  /* Scroll on buttons */
  $(".js--scroll-to-plans").click(function() {
    $("html, body").animate(
      { scrollTop: $(".js--section-plans").offset().top },
      1000
    );
  });

  $(".js--scroll-to-start").click(function() {
    $("html, body").animate(
      { scrollTop: $(".js--section-features").offset().top },
      1000
    );
  });

  /* Navigation Scrolls */
  $('a[href*="#"]')
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        if (target.length) {
          event.preventDefault();
          $("html, body").animate(
            {
              scrollTop: target.offset().top
            },
            1000,
            function() {
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) {
                return false;
              } else {
                $target.attr("tabindex", "-1");
                $target.focus();
              }
            }
          );
        }
      }
    });

  /* Animations on scroll */
  $(".js--wp-1").waypoint(
    function(direction) {
      $(".js--wp-1").addClass("animated fadeIn");
    },
    { offset: "50%" }
  );
});

/* Animations on scroll */
$(".js--wp-1").waypoint(
  function(direction) {
    $(".js--wp-1").addClass("animated fadeIn");
  },
  { offset: "50%" }
);

/* Animations on scroll */
$(".js--wp-2").waypoint(
  function(direction) {
    $(".js--wp-2").addClass("animated fadeInUp");
  },
  { offset: "50%" }
);

/* Animations on scroll */
$(".js--wp-3").waypoint(
  function(direction) {
    $(".js--wp-3").addClass("animated fadeIn");
  },
  { offset: "50%" }
);

/* Animations on scroll */
$(".js--wp-4").waypoint(
  function(direction) {
    $(".js--wp-4").addClass("animated pulse");
  },
  { offset: "50%" }
);

/* Mobile navigation */
$(".js--nav-icon").click(function() {
  let nav = $(".js--main-nav");
  let icon = $(".js--nav-icon i");

  nav.slideToggle(200);

  if (icon.hasClass("ion-ios-menu")) {
    icon.removeClass("ion-ios-menu");
    icon.addClass("ion-ios-close");
  } else {
    icon.removeClass("ion-ios-close");
    icon.addClass("ion-ios-menu");
  }
});
